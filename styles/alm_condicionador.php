<?php require_once('img/imagens.php'); ?>
<body class="alm_shampoo">
	<div class="rotulo">
		<div class="qrcode">
			<img src="{qrcode}"/>
		</div>
		<div class="persona">
			<div class="wrap" style="{scalep}">
				<div class="name"><p>Condicionador<p></div>
				<div class="desc"><p>anti-queda</p></div>
			</div> 
			<div class="bottom">				
				<div class="separador"></div>
				<div class="qtdy"><p>{qtdy}</p></div>
			</div>
		</div>
		<?php // echo '<div style="font-size:4px; text-align:center; color:white">{scalep}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{scale}</div>' ?>
		<div class="infos" style="{scale}">
			<div class="top">
				<div class="separador icon"><img src=<?php echo $icone_almofariz ?> /></div>
				<div class="name"><p>CONDICIONADOR ANTI-QUEDA</p></div>
				<div class="componentes"><p><b>COMPOSIÇÃO:</b> {componentes}</p></div>
				<div class="posologia"><p><b>MODO DE USO:</b>{posologia}</p></div>
			</div>
			<div class="bottom">
				<div class="separador icon"><img src=<?php echo $icon ?> /></div>
				<div class="name"><p>{namereg}</p></div>
				<div class="regreq">
					<span class="reg">{reg}</span>
					<span class="req">{req}</span>
				</div>
				<div class="manval">
					<span class="man">{man} </span>
					<span class="val">{val}</span>
				</div>
				<div class="farmresp"><p><b>Farm. Resp.:</b>{farmresp}</p></div>
				<div class="address">
					<div class="bottom"><p>{matriz}</p></div>		
				</div>
			</div>
		</div>
		
	</div>
</body>

<?php
	class AutoSize{
		public function __toString(){
			return __FILE__;
		}
		public function scalePersona($str){
			$size = strlen($str) - 82;
			$w		= 4.8;
			$h		= 2.9;

			if($size <= 40){ 					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			if($size >= 41 && $size <= 50){ 	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 51 && $size <= 65){		$s = 0.8; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 65){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
		public function scaleComponentes($str){
			$size 	= strlen($str) - 82;
			$w		= 4.3;
			$h		= 5.74;
			
			if($size <= 430){					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			if($size >= 431 && $size <= 580){	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 581 && $size <= 760){	$s = 0.8;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 761 && $size <= 860){	$s = 0.75;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 861){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
	}
	
?>