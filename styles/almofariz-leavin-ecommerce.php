<?php require_once('img/imagens.php'); ?>
<body class="alm_shampoo">
	<div class="rotulo">
		<div class="qrcode">
			<img src="{qrcode}"/>
		</div>
		<div class="persona">
			<div class="wrap">
				<div class="name">{tituloFormula}</div>
				<div class="desc">{ativosFormula}</div>
				<div class="separador"></div>
				<div class="qtdy">{qtdy}</div>
			</div>
		</div>
		<div class="infos" style="{scale}">
			<div class="top">
				<div class="separador icon"><img src=<?php echo $icone_almofariz ?> /></div>
				<div class="componentes" style="{componentes_file_size}"><b>COMPOSIÇÃO:</b> {componentes}</div>
				<div class="posologia" style="{posologia_file_size}"><b>MODO DE USO:</b>{posologia}</div>
			</div>
			<div class="bottom">
				<div class="separador icon"><img src=<?php echo $icon ?> /></div>
				<div class="name" style="{name_reg_file_size}">{namereg}</div>
				<div class="regreq">
					<span class="reg">{reg}</span>
					<span class="req">{req}</span>
				</div>
				<div class="manval">
					<span class="man">{man} </span>
					<span class="val">{val}</span>
				</div>
				<div class="farmresp"><b>Farm. Resp.:</b>{farmresp}</div>
				<div class="address">
					<div class="bottom">{matriz}</div>		
				</div>
			</div>
		</div>
		
	</div>
</body>


<?php
	class AutoSize{
		public function __toString(){
			return __FILE__;
		}
		public function scaleComponentes($str){
			$size = strlen($str);
			
			if($size <= 239){
				return 'transform: scale(1.0)';
			}
			if($size >= 240 && $size <= 640){
				return 'transform: scale(0.9); width:4.7cm;';
			}
			if($size >= 641 && $size <= 840){
				return 'transform: scale(0.8); width:5.3cm;';
			}
			if($size > 841){
				return 'transform: scale(0.6); width:6cm;';
			}
		}
	}
?>
