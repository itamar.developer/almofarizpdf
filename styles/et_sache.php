<?php require_once('img/imagens.php'); ?>
<body class="et_sache">
	<div class="rotulo">
		<div class="qrcode">
			<img src="{qrcode}"/>
		</div>
		<?php // echo '<div style="font-size:4px; text-align:center; color:black;">{scalep}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{scale}</div>' ?>
		<div class="infos" style="{scale}">
			<div class="top">
				<div class="separador"></div>
				<div class="name"><p>{namereg}</div>
				<div class="regreq">
					<span class="reg">{reg}</span>
					<span class="req" style="margin-left:27px;">{req}</span>
				</div>
                <div class="manval">
					<span class="man">{man} </span>
					<span class="val" style="margin-left:11px;">{val}</span>
				</div>
				<div class="componentes"><p><strong>COMPOSIÇÃO:</strong> {componentes}</p></div>
				<div class="posologia"><p>{posologia}<br/><br/><strong>CONTÉM:</strong> {qtdy}.</p></div>
			</div>
			<div class="bottom">
				<div class="separador"></div>
                <div class="doctor"><p>{doctor}</p></div>
				<div class="crm">{crm}</div>
				<div class="farmresp"><p><b>FARM. RESP.:</b><span>{farmresp}</span></p></div>
				<div class="address">
					<div class="separador"></div>
					<div class="bottom">{matriz}</div>		
				</div>
			</div>
		</div>
		
	</div>
</body>

<?php
	class AutoSize{
		public function __toString(){
			return __FILE__;
		}
		public function scalePersona($str){
			$size = strlen($str) - 82;
			$w		= 3.2;
			$h		= 1.07;

			// if($size <= 18){ 					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			// if($size >= 18 && $size <= 25){		$s = 0.8; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			// if($size >= 26){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
		public function scaleComponentes($str){			
			$size 	= strlen($str) - 82;
			$w		= 3.9;
			$h		= 5.5;

			if($size <= 420){					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			if($size >= 421 && $size <= 590){	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 591 && $size <= 720){	$s = 0.8;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 721 && $size <= 860){	$s = 0.75;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 861){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
	}
	
?>