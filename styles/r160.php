<?php require_once('img/imagens.php'); ?>
<body class="r160">
    <div class="rotulo">
        <div class="qrcode">
            <img src="{qrcode}"/>
        </div>
        <div class="persona">
			<div class="wrap">
				<div class="name">
					<p>
						<div class="icon">
							<img src="<?php echo $icon?>" />
						</div>
						<span class="all" style={scalename}>{name}</span>
					</p>
				</div>
				<div class="posologiaDesc">
					<div class="posologiascale" style="{scaleposologia}">
						<div class="header-posologia">
							<img src="<?php echo $icone_posologia;?>" />
							<h3>Orientação farmacêutica</h3>
						</div>
						<p>{posologiaDesc}</p>
					</div>
				</div>
			</div>
			<div class="bottom">
				<div class="qtdy"><p>{qtdy}</p></div>
			</div>
		</div>
		<?php // echo '<div style="font-size:4px; text-align:center;">{scalep}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{scale}</div>' ?>
		<div class="infos"  style="{scale}">
			<div class="top" >
				<div class="separador icon"><img src=<?php echo $icon ?> /></div>
				<div class="name"><p>{namereg}</p></div>
				<div class="regreq" >
					<span>{reg} <span style="display: inline; margin-left:20px;">{req}</span></span>
				</div>
				<div class="manval" >
					<span class="man">{man} </span>
					<span class="val" style="margin-left:9px;">{val}</span>
				</div>
				<div class="componentes"><p><strong>COMPOSIÇÃO:</strong> {componentes}</p></div>
				<div class="posologia"><p> {posologia}</p></div>
			</div>
			<div class="bottom">
				<div class="separador icon"><img src=<?php echo $icone_almofariz ?> /></div>				
				<div class="doctor"><p>{doctor}</p></div>
				<div class="crm"><p>{crm}</div>
				<div class="farmresp"><p><b>FARM. RESP.:</b>{farmresp}</p></div>
			</div>
		</div>
        <div class="address">
            <div class="bottom">{matriz}</div>      
        </div>
        <div></div>
    </div>
</body>


<?php
	class AutoSize{
		public function __toString(){
			return __FILE__;
		}
		public function scalePersona($str){
			$size = strlen($str) - 82;
			$w		= 4.60;
			$h		= 3.18;

			if($size <= 70){ 					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			if($size >= 71 && $size <= 90){ 	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 91 && $size <= 120){	$s = 0.8; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 121){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
		public function scaleComponentes($str){
			$size 	= strlen($str) - 82;
			$w		= 4.65;
			$h		= 5.6;
			
			if($size <= 410){					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			if($size >= 411 && $size <= 510){	$s = 0.95; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 511 && $size <= 660){	$s = 0.9;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 661 && $size <= 840){	$s = 0.85;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 841){					$s = 0.8;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}

		public function scaleName($str){
			$size 	= strlen($str);


			if($size <= 20){ 				    return 'font-size:9pt' ;}
			if($size >= 20 && $size <= 30){     return 'font-size:7pt' ;}
			if($size >= 30 && $size <= 40){	return 'font-size:6pt';}
			if($size >= 40 && $size <= 50){	return 'font-size:5pt';}
			if($size >= 50){					return 'font-size:4pt';}
		}
		
		public function scalePosologia($str){
			$size 	= strlen($str);
			if($size <= 0){ return 'display:none;'; }

			if($size <= 35){ 				    return 'font-size:8pt;' ;}
			if($size >= 35 && $size <= 70){     return 'font-size:7pt' ;}
			if($size >= 70 && $size <= 105){     return 'font-size:6.5pt' ;}
			if($size >= 105){					return 'font-size:6pt;';}
		}

		public function limitCaracterPosologia(){
			return 180;
		}
		
	}
	
?>