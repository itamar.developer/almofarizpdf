<?php require_once('img/imagens.php'); ?>
<body class="pump_p">
	<div class="rotulo">
		<div class="qrcode">
			<img src="{qrcode}"/>
		</div>
		<div class="persona">
			<div class="wrap" style="{scalep}">
				<div class="icon"><img src="<?php echo $icon?>" /></div>
				<div class="name"><p><span class="for">para </span>{name}</p></div>
			</div>
			<div class="bottom">				
				<div class="separador"></div>
				<div class="qtdy"><p>{qtdy}</p></div>
			</div>
		</div>
		<?php //echo '<div style="font-size:4px; text-align:center;">{scalep}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{scale}</div>' ?>		
		<div class="infos" style="{scale}">
			<div class="top">
				<div class="separador icon"><img src=<?php echo $icon ?> /></div>
				<div class="name"><p>{namereg}</p></div>
				<div class="regreq">
					<span class="reg">{reg} <span style="display:inline; margin-left:30px;">{req}</span></span>
				</div>
                <div class="manval">
					<span>{man} </span>
					<span style="display:inline; margin-left:13px;">{val}</span>
				</div>
				<div class="componentes"><p><strong>COMPOSIÇÃO:</strong> {componentes}</p></div>
				<div class="posologia"><p> {posologia}</p></div>
			</div>
			<div class="bottom">
				<div class="separador icon"><img src=<?php echo $icone_almofariz ?> /></div>
				<div class="doctor"><p>{doctor}</p></div>
				<div class="crm">{crm}</div>
				<div class="farmresp"><p><b>Farm. Resp.:</b>{farmresp}</p></div>
			</div>
		</div>
		<div class="address">
			<div class="bottom">{matriz}</div>		
		</div>
	</div>
</body>


<?php
	class AutoSize{
		public function __toString(){
			return __FILE__;
		}
		public function scalePersona($str){
			$size = strlen($str) - 82;
			$w		= 2.5;	
			$h		= 3.7;

			if($size <= 40){ 					$s = 1; 	return 'transform:rotate(-90deg) scale('.$s.')'											.'size:'.$size	;}
			if($size >= 41 && $size <= 60){ 	$s = 0.9; 	return 'transform:rotate(-90deg) scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 61 && $size <= 80){		$s = 0.8; 	return 'transform:rotate(-90deg) scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 81){					$s = 0.7;	return 'transform:rotate(-90deg) scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
		public function scaleComponentes($str){
			$size 	= strlen($str) - 82;
			$w		= 3.8;
			$h		= 5.7;
	
			if($size <= 500){					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			if($size >= 501 && $size <= 700){	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 701 && $size <= 870){	$s = 0.8;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 871 && $size <= 980){	$s = 0.75;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 981){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
	}
	
?>