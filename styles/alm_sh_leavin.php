<?php require_once('img/imagens.php'); ?>
<body class="alm_shampoo">
	<div class="rotulo">
		<div class="qrcode">
			<img src="{qrcode}"/>
		</div>
		<div class="persona">
			<div class="wrap">
				<div class="name">Leave-In</div>
				<div class="desc">restauração total</div>
                <div class="separador"></div>
                <!-- <div class="sub">com <span>vitamina C</span></div> -->
				<div class="qtdy">{qtdy}</div>
			</div>
		</div>
		<div class="infos">
			<div class="top">
				<div class="separador icon"><img src=<?php echo $icone_almofariz ?> /></div>
				<div class="name" style="{name_reg_file_size}">SHAMPOO ANTI-QUEDA</div>
				<div class="componentes" style="{componentes_file_size}"><b>COMPOSIÇÃO:</b> {componentes}</div>
				<div class="posologia" style="{posologia_file_size}"><b>MODO DE USO:</b>{posologia}</div>
			</div>
			<div class="bottom">
				<div class="separador icon"><img src=<?php echo $icon ?> /></div>
				<div class="name" style="{name_reg_file_size}">{namereg}</div>
				<div class="regreq">
					<span class="reg">{reg}</span>
					<span class="req">{req}</span>
				</div>
				<div class="manval">
					<span class="man">{man} </span>
					<span class="val">{val}</span>
				</div>
				<div class="farmresp"><b>Farm. Resp.:</b>{farmresp}</div>
				<div class="address">
					<div class="bottom">{matriz}</div>		
				</div>
			</div>
		</div>
		
	</div>
</body>


<?php
	class AutoSize{
		public function __toString(){
			return __FILE__;
		}
		public function scalePersona($str){
			$size = strlen($str) - 82;
			$w		= 6.18;
			$h		= 4;

			// if($size <= 70){ 					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			// if($size >= 71 && $size <= 90){ 	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			// if($size >= 91 && $size <= 120){	$s = 0.8; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			// if($size >= 121){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
		public function scaleComponentes($str){
			$size 	= strlen($str) - 82;
			$w		= 5.05;
			$h		= 7.3;
			
			// if($size <= 390){					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			// if($size >= 391 && $size <= 510){	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			// if($size >= 511 && $size <= 660){	$s = 0.8;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			// if($size >= 661 && $size <= 830){	$s = 0.75;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			// if($size >= 831){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
	}
	
?>