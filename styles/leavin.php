<?php require_once('img/imagens.php'); ?>
<body class="leavin">
	<div class="rotulo">
		<div class="qrcode">
			<img src="{qrcode}"/>
		</div>
		<div class="persona">
			<div class="wrap" style="{scalep}">
				<div class="icon"><img src="<?php echo $icon?>" /></div>
				<div class="name"><p><span class="for">para</span><span class="all">{name}</span></p></div>
            </div>
			<div class="bottom">				
				<div class="separador"></div>
				<div class="qtdy"><p>{qtdy}</p></div>
			</div>
		</div>
		<?php // echo '<div style="font-size:4px; text-align:center;">{scalep}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{scale}</div>' ?>
		<div class="infos"  style="{scale}">
			<div class="top" >
				<div class="separador icon"><img src=<?php echo $icon ?> /></div>
				<div class="name"><p>{namereg}</p></div>
				<div class="regreq" >
					<span>{reg} <span style="display: inline; margin-left:20px;">{req}</span></span>
				</div>
				<div class="manval" >
					<span class="man">{man} </span>
					<span class="val" style="margin-left:9px;">{val}</span>
				</div>
				<div class="componentes"><p><strong>COMPOSIÇÃO:</strong> {componentes}</p></div>
				<div class="posologia"><p> {posologia}</p></div>
			</div>
			<div class="bottom">
				<div class="separador icon"><img src=<?php echo $icone_almofariz ?> /></div>				
				<div class="doctor"><p>{doctor}</p></div>
				<div class="crm"><p>{crm}</div>
				<div class="farmresp"><p><b>FARM. RESP.:</b>{farmresp}</p></div>
			</div>
		</div>
		<div class="address">
			<div class="bottom">{matriz}</div>		
		</div>
		<div></div>
	</div>
</body>


<?php
	class AutoSize{
		public function __toString(){
			return __FILE__;
		}
		public function scalePersona($str){
			$size = strlen($str) - 82;
			$w		= 4.05;
			$h		= 2.84;

			if($size <= 60){ 					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			if($size >= 61 && $size <= 90){ 	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 91 && $size <= 120){	$s = 0.8; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 121){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
		public function scaleComponentes($str){
			$size 	= strlen($str) - 82;
			$w		= 4.17;
			$h		= 4.9;
		
			if($size <= 460){					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			if($size >= 461 && $size <= 570){	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 571 && $size <= 790){	$s = 0.8;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 791 && $size <= 1100){	$s = 0.75;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 1101){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
	}
	
?>
