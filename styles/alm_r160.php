<?php require_once('img/imagens.php'); ?>
<body class="alm_r160">
	<div class="rotulo">
		<div class="qrcode">
			<img src="{qrcode}"/>
		</div>
		<div class="persona">
			<div class="wrap" style="{scalep}">
				<div class="name"><p>{tituloFormula}</p></div>
				<div class="desc"><p>{ativos_formula}</p></div>
			</div>
			<div class="bottom">
				<div class="separador"></div>
				<div class="qtdy"><p>{qtdy}</p></div>
			</div>
		</div>
		<?php // echo '<div style="font-size:4px; text-align:center;">{scalep}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{scale}</div>' ?>
		<div class="infos" style="{scale}">
			<div class="top">
				<div class="separador icon"><img src=<?php echo $icone_almofariz ?> /></div>
				<div class="componentes"><p><b>COMPOSIÇÃO:</b> {componentes}</p></div>
				<div class="posologia"><p> {posologia}</p></div>
			</div>
			<div class="bottom">
				<div class="separador icon"><img src=<?php echo $icon ?> /></div>
				<div class="name"><p>{namereg}</p></div>
				<div class="regreq">
					<span class="reg">{reg}</span>
					<span class="req">{req}</span>
				</div>
				<div class="manval">
					<span class="man">{man} </span>
					<span class="val">{val}</span>
				</div>
				<div class="farmresp"><p><b>Farm. Resp.:</b>{farmresp}</p></div>
				<div class="address">
					<div class="bottom">{matriz}</div>		
				</div>
			</div>
		</div>
	</div>
</body>



<?php
	class AutoSize{
		public function __toString(){
			return __FILE__;
		}
		public function scalePersona($str){
			$size = strlen($str) - 82;
			$w		= 4.95;
			$h		= 3;

			if($size <= 70){ 					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			if($size >= 71 && $size <= 90){ 	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 91 && $size <= 120){	$s = 0.8; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 121){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
		public function scaleComponentes($str){
			$size 	= strlen($str) - 82;
			$w		= 4.5;
			$h		= 5.7;
			
			if($size <= 280){					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			if($size >= 281 && $size <= 480){	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 481 && $size <= 660){	$s = 0.8;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 661 && $size <= 800){	$s = 0.75;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 801){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
	}
	
?>