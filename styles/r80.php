<?php require_once('img/imagens.php'); ?>
<body class="r80">
	<div class="rotulo">
		<div class="qrcode">
			<img src="{qrcode}"/>
		</div>
		<div class="persona">
			<div class="wrap">
				<div class="name">
					<p>
						<div class="icon">
							<img src="<?php echo $icon?>" />
						</div>
						<span class="all" style={scalename}>{name}</span>
					</p>
				</div>
				<div class="posologiaDesc">
					<div class="posologiascale" style="{scaleposologia}">
						<div class="header-posologia">
							<img src="<?php echo $icone_posologia;?>" />
							<h3>Orientação farmacêutica</h3>
						</div>
						<p>{posologiaDesc}</p>
					</div>
				</div>
			</div>
			<div class="bottom">
				<div class="qtdy"><p>{qtdy}</p></div>
			</div>
		</div>
		<?php //echo '<div style="font-size:4px; text-align:center;">{scaleposologia}</div>' ?>		
		<div class="infos" style="{scale}">
			<div class="top">
				<div class="separador icon"><img src=<?php echo $icon ?> /></div>
				<div class="name"><p>{namereg}</p></div>
				<div class="regreq">
					<span>{reg} <span style="display: inline; margin-left:25px;">{req}</span></span>
				</div>
				<div class="manval">
					<span class="man">{man} </span>
					<span class="val" style="margin-left:9px;">{val}</span>
				</div>
				<div class="componentes"><p><strong>COMPOSIÇÃO:</strong> {componentes}</p></div>
				<div class="posologia"><p> {posologia}</p></div>
			</div>
			<div class="bottom">
				<div class="separador icon"><img src=<?php echo $icone_almofariz ?> /></div>
				<div class="doctor"><p>{doctor}</p></div>
				<div class="crm">{crm}</div>
				<div class="farmresp"><p><b>FARM. RESP.:</b>{farmresp}</p></div>
			</div>
		</div>
		<div class="address">
			<div class="bottom">{matriz}</div>		
		</div>
	</div>
</body>


<?php
	class AutoSize{
		public function __toString(){
			return __FILE__;
		}
		public function scalePersona($str){
			$size = strlen($str) - 82;
			$w		= 2.25;
			$h		= 3.06;

			if($size <= 50){ 					$s = 1; 	return 'transform: scale('.$s.')'										;}
			if($size >= 51 && $size <= 70){ 	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm';}
			if($size >= 71 && $size <= 90){		$s = 0.8; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm';}
			if($size >= 91){					$s = 0.4;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm';}
		}

		
		public function scaleComponentes($str){
			$size 	= strlen($str) - 82;
			$w		= 5.7;
			$h		= 3.4;
			
			if($size <= 360){					$s = 1; 	return 'transform: scale('.$s.')'										;}
			if($size >= 361 && $size <= 500){	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm';}
			if($size >= 501 && $size <= 660){	$s = 0.8;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm';}
			if($size >= 661 && $size <= 830){	$s = 0.75;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm';}
			if($size >= 831){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm';}
		}
		

		public function scaleName($str){
			$size 	= strlen($str);


			if($size <= 20){ 				    return 'font-size:9pt' ;}
			if($size >= 20 && $size <= 30){     return 'font-size:7pt' ;}
			if($size >= 30 && $size <= 40){		return 'font-size:6pt';}
			if($size >= 40 && $size <= 50){		return 'font-size:5pt';}
			if($size >= 50){					return 'font-size:4pt';}
		}

		public function scalePosologia($str){
			$size 	= strlen($str);
			if($size <= 0){ return 'display:none;'; }
			if($size <= 35){ 				    return 'font-size:7pt;' ;}
			if($size >= 35 && $size <= 70){     return 'font-size:5pt' ;}
			if($size >= 70 && $size <= 105){     return 'font-size:4pt' ;}
			if($size >= 105){					return 'font-size:3.7pt;';}
		}
		
		public function limitCaracterPosologia(){
			return 180;
		}
	}
	
?>