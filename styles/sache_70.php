<?php require_once('img/imagens.php'); ?>
<body class="sache_70">
	<div class="rotulo">
		<div class="sabiamais">Saiba mais sobre<br /> sua fórmula. <img class="arrow" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAyCAYAAAAayliMAAALrmlDQ1BJQ0MgUHJvZmlsZQAASImVVwdUU8kanltSCEkoCVVKAOmiAaRLCR2R3gQbIQkkEEIMAQU7Iq7gWhARwbICqyCKrgWQtWLBwiLY+wMRFWVdLNhQeRPAsu+d9955kzP3/+bLP98//8zce2YAoD3iSqViVBWAdIlcFhngzZoen8Ai9QICYAIAnAGby8uUcsLDp8EW+Gr/Xt7eAIjCXrVRaP37//+1qPEFmTwAkHCIk/iZvHSIDwGANvOkMjkAuBTyJvPkUgUugZgpgwOEuEaBU0bxUQVOGsUdIz7RkT4QPwKATOVyZSkAUAcgz8rmpUAdGswWsCV8kQRiL4g9eEIuH+KlEE+APjAmbY8i+6QfdFL+ppn0TZPLTfmGR3MZKWRfUaZUzM35P6fjf5d0cdbXGAawUmVZkTHQ0uG8zc9MiwoewwU8rl8UxDBXpEQoCFKsGw5xtVTuHTnmU5Ms848Z82kQZgV+1TknkgdFj+FraRnBCn8KxA95mT4JX/tKkkLDFPEh7s9Ki+GM8e/5Al+/rz6yjMgxH5SYK4yOg5gIsXpmdpTCRx1i7VyhT+gYb5XKnRo+5s8WiAPG4qJTpPLwsG9xxaGKXDQh7y/I/J6jXBgdOIrRmXK4Acb6ipNF/kFjmtlCWWDkGM6Tikf28ogmV+YXoNg3kF8jkMREjfJoJZ/rGzymsx/Xwz1wd9wZ94bWC/gCEcgEUiAGApAKwgEXSIAcPsXwxyU8JlwBISADciLIZgDZDy0ByILtYIhTIRaDXCQCRAEeEMKWDKSDjMPMlurr1CZyqxzyaSNekMeZMDYH98TdcAfcBXBAMhChOjBiallGjmusEHhDRTHgw1GlgUeKHskeCp7dyq5iH2HfZPezq7HtWBPWhh3DGqG3YCS6wv9bbDqZbkPn0K3oTDqJrkc3BX5QSzaSWyjUzIGjl4O3IAEymbAvzIg4mWgOqyVgQetLdCHaEh1/mBsu7KPI4fFIZkAumC9XbFyfDGmOTJQilLM48IshYAVJeBMnsOzYdrYAKL4/o9v7dcTIdwXRbPvOyeF6uP8JANbxnUvIBGAP1NWy/85ZaAOgsQ2AJitelix7lMMVDwJcTRW40jrw7TEBFsAG2AFH4Aa8YK5TQRiIBvFg9siMpMMxzwMLwTJQAIrAOrARlIPtoArUgL3gAGgER8EpcA5cAh3gOrgLukAveA4G4OwMIQhCQmgIA9FBDBFTxBqxQ5wRD8QPmYZEIvFIIpKCSJAsZCGyHClCipFyZAdSi/yGHEFOIReQTuQ20o30Ia+QjyiGUlEmqo+aoZNQZ5SDBqPR6Cw0BZ2L5qL56Bq0DK1E96AN6Cn0Enod7UKfo4MYwJQxTcwIs8GcMR8sDEvAkjEZthgrxEqxSqwea8ZasatYF9aPfcCJOANn4TZwRwXiMTgPn4svxlfj5XgN3oCfwa/i3fgA/oVAI+gRrAmuhCDCdEIKYR6hgFBK2Ek4TDhLuE7oJbwlEomacO2diIHEeGIqcQFxNXErcR/xJLGT2EMcJJFIOiRrkjspjMQlyUkFpM2kPaQTpCukXtJ7sjLZkGxH9icnkCXkPHIpeTf5OPkK+Ql5SElVyVTJVSlMia+Uo7RWqVqpWemyUq/SEEWNYk5xp0RTUinLKGWUespZyj3Ka2VlZWNlF+UIZZHyUuUy5f3K55W7lT9Q1alWVB/qTGoWdQ11F/Uk9Tb1NY1GM6N50RJoctoaWi3tNO0B7T2dQZ9ID6Lz6UvoFfQG+hX6CxUlFVMVjspslVyVUpWDKpdV+lWVVM1UfVS5qotVK1SPqN5UHVRjqNmqhamlq61W2612Qe2pOkndTN1Pna+er16lflq9h4ExTBg+DB5jOaOacZbRyyQyzZlBzFRmEXMvs505oKGuMVkjVmO+RoXGMY0uTUzTTDNIU6y5VvOA5g3Nj1r6WhwtgdYqrXqtK1rvtMdpe2kLtAu192lf1/6ow9Lx00nTWa/TqHNfF9e10o3Qnae7Tfesbv845ji3cbxxheMOjLujh+pZ6UXqLdCr0mvTG9Q30A/Ql+pv1j+t32+gaeBlkGpQYnDcoM+QYehhKDIsMTxh+IylweKwxKwy1hnWgJGeUaBRltEOo3ajIWNz4xjjPON9xvdNKCbOJskmJSYtJgPjDceHjF84vm78HVMlU2dToekm01bTd2bmZnFmK80azZ6aa5sHmeea15nfs6BZeFrMtai0uGZJtHS2TLPcatlhhVo5WAmtKqwuW6PWjtYi663WnRMIE1wmSCZUTrhpQ7Xh2GTb1Nl0T9ScOG1i3sTGiS8mjZ+UMGn9pNZJX9gObDG7mn3XVt12qm2ebbPtKzsrO55dhd01e5q9v/0S+yb7l5OtJwsmb5t8y4HhEOKw0qHF4bOjk6PMsd6xz2m8U6LTFqebzkzncOfVzuddCC7eLktcjrp8cHV0lbsecP3LzcYtzW2329Mp5lMEU6qn9Lgbu3Pdd7h3ebA8Ej1+8ejyNPLkelZ6PvQy8eJ77fR6wrHkpHL2cF54s71l3oe93/m4+izyOemL+Qb4Fvq2+6n7xfiV+z3wN/ZP8a/zHwhwCFgQcDKQEBgcuD7wZpB+EC+oNmhgqtPURVPPBFODo4LLgx9Os5omm9YcgoZMDdkQci/UNFQS2hgGwoLCNoTdDzcPnxv+ewQxIjyiIuJxpG3kwsjWKEbUnKjdUW+jvaPXRt+NsYjJimmJVYmdGVsb+y7ON644rmv6pOmLpl+K140XxTclkBJiE3YmDM7wm7FxRu9Mh5kFM2/MMp81f9aF2bqzxbOPzVGZw51zMJGQGJe4O/ETN4xbyR1MCkrakjTA8+Ft4j3ne/FL+H0Cd0Gx4Emye3Jx8tMU95QNKX1CT2GpsF/kIyoXvUwNTN2e+i4tLG1X2rA4TrwvnZyemH5Eoi5Jk5zJMMiYn9EptZYWSLvmus7dOHdAFizbmYlkzspskjPhQa8tyyJrRVZ3tkd2Rfb7ebHzDs5Xmy+Z35ZjlbMq50muf+6vC/AFvAUtC40WLlvYvYizaMdiZHHS4pYlJkvyl/QuDVhas4yyLG3ZH3nsvOK8N8vjljfn6+cvze9ZEbCiroBeICu4udJt5faf8J9EP7Wvsl+1edWXQn7hxSJ2UWnRp9W81Rd/tv257OfhNclr2tc6rt22jrhOsu7Ges/1NcVqxbnFPRtCNjSUsEoKS95snLPxQunk0u2bKJuyNnWVTStr2jx+87rNn8qF5dcrvCv2bdHbsmrLu638rVe2eW2r366/vWj7x19Ev9zaEbCjodKssrSKWJVd9bg6trr1V+dfa3fq7iza+XmXZFdXTWTNmVqn2trdervX1qF1WXV9e2bu6djru7ep3qZ+xz7NfUX7wf6s/c9+S/ztxoHgAy0HnQ/WHzI9tOUw43BhA9KQ0zDQKGzsaopv6jwy9UhLs1vz4d8n/r7rqNHRimMax9YepxzPPz58IvfE4Enpyf5TKad6Wua03D09/fS1MxFn2s8Gnz1/zv/c6VZO64nz7uePXnC9cOSi88XGS46XGtoc2g7/4fDH4XbH9obLTpebOlw6mjundB6/4nnl1FXfq+euBV27dD30eueNmBu3bs682XWLf+vpbfHtl3ey7wzdXXqPcK/wvur90gd6Dyr/YfmPfV2OXce6fbvbHkY9vNvD63n+KPPRp978x7THpU8Mn9Q+tXt6tM+/r+PZjGe9z6XPh/oL/lT7c8sLixeH/vL6q21g+kDvS9nL4VerX+u83vVm8puWwfDBB2/T3w69K3yv877mg/OH1o9xH58MzftE+lT22fJz85fgL/eG04eHpVwZd+QogMGKJicD8GoXPOvHA8CA9wbKjNH7wUhBRu80Iwj8Jzx6hxgpjgDsgOfPOHhWDF4BQHk3AOb1ULcBgHAaANEuALW3/1bHSmayvd2oFtUbHk0eDA+/NgOAtAGAz+uGh4cqh4c/V8HB3gPgpGT0XvLD2P/VAoXyiPbf7D8Bji8+x/0OVLQAAACKZVhJZk1NACoAAAAIAAQBGgAFAAAAAQAAAD4BGwAFAAAAAQAAAEYBKAADAAAAAQACAACHaQAEAAAAAQAAAE4AAAAAAAAAkAAAAAEAAACQAAAAAQADkoYABwAAABIAAAB4oAIABAAAAAEAAAAwoAMABAAAAAEAAAAyAAAAAEFTQ0lJAAAAU2NyZWVuc2hvdONhTVIAAAAJcEhZcwAAFiUAABYlAUlSJPAAAAHUaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA2LjAuMCI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIj4KICAgICAgICAgPGV4aWY6UGl4ZWxZRGltZW5zaW9uPjUwPC9leGlmOlBpeGVsWURpbWVuc2lvbj4KICAgICAgICAgPGV4aWY6UGl4ZWxYRGltZW5zaW9uPjQ4PC9leGlmOlBpeGVsWERpbWVuc2lvbj4KICAgICAgICAgPGV4aWY6VXNlckNvbW1lbnQ+U2NyZWVuc2hvdDwvZXhpZjpVc2VyQ29tbWVudD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+Cj8XgdwAAAAcaURPVAAAAAIAAAAAAAAAGQAAACgAAAAZAAAAGQAAATlmZxIlAAABBUlEQVRoBexWvQ5EQBCeDYXeEyg8gSfwAl6Jd5BoNSKiVngRhVZCI0oFzdythMje3imsuLvMJBIzye7M97MWw2fADwcjADerRwrcLACQAn+rQFVVUJYlWJYFnueBruvXYOWfUdURxzEyxvj9sjyu6+I8z6rbLPuB6l35oKZpbsOvILIsU93qGgBd170Mz0EEQfAbANq2lQLwfZ8AyBhQfgZIARnNH2qkgEgOWUhk5CAnC4kEkYVERg5yspBI0FdZaBxHTJIEoyjCpmnEWaX5WQDTNGGe5xiGIdZ1Le2xL761UN/3aNv29mNmGAYWRbFfK30/A2AYBnQcZ+upaRqmaSrtsxYfAAAA///U5eXGAAACXklEQVTtl0mKIkEUhrNBcFg6u3Dt2gEn8AZqqaioCN7BjUs9gXcQvEKJ4xWcygn3bp12DvjK101IS+XLCtNsoaEC0sjMiPf//xeZBioA0SqVCgiCcHeo1Wp4f38nKv7cXq1WdzVMo1wuS9at12twOp1fau12u2SdQI0WCoUvYhgGIRqNBlUGcgAwvMvlEvVDz9PpRPqRALVajRREiGazKSr6KMBmswG32016hUIhUR92kwS4XC6Qz+dJYY1GA61Wi+nc+kcAttsteDwe0sNms8Fyubxpi52QADj5fD5DLpcjDRCi3W7f6fICYHiv10tqY/jFYnGnLXYhCYAFCJHNZkkjrVYLnU7nps0DsNvtwOfzkZpWq5UrPJp+C4CTECKTyZCGCNHtdnHqt19iDO/3+0ktDD+fz39r8XxwAaAQQqTTadIYIXq9niTAfr+HQCBAalgsFpjNZjy5b3O4AbACt7NUKkUG0Ol0UK/XRceLxSIEg0HRMdwqzWYzTKfTWzDek4cAGEQymSSDqFQq0THq/jPhMc/DADwQGIr3wJWfTCYoK6vJAkAnfJ0SiQR3UDEgk8kEHx8fsoKzItkAKHA8HiEej8uCMBqNMB6PWQ7Z/VMADCIWiz0EoVR49H8agEG8vb1xQRgMBhiNRlimSFMEAJPg6xSNRiUhMPxwOFQkOBNRDAAFD4cDRCIRUQi9Xg+DwYD5KtYrCsAgwuHwHQSG7/f7ioX+W+gXXly3OEXbdYsVqtWqcP19JFz/UQmlUklwOByKejCxfwLAxF/R/wC8YpWlPH6egNTqvGLsv38Cn6yzuH4r3DxxAAAAAElFTkSuQmCC" />	 </div>
		<div class="qrcode">
			<img src="{qrcode}"/>
		</div>
		<div class="persona">
			<div class="wrap" style="{scalep}">
				<div class="icon"><img src="<?php echo $icon?>" /></div>
				<div class="name"><p><span class="for">para </span><span class="all">{name}</span></p></div>
			</div>
		</div>
		<?php // echo '<div style="font-size:4px; text-align:center; color:black;">{scalep}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{scale}</div>' ?>
		<div class="infos" style="{scale}">
			<div class="separador"></div>
			<div class="top">
				<div class="regreq" style="margin-top:-5px">
					<span class="reg">{reg}</span>
					<span class="req">{req}</span>
				</div>
				<div class="manval" style="margin-top:-3px">
					<span class="man">{man}</span>
					<span class="val" style="margin-left:-13px">{val}</span>
				</div>
				<div class="componentes" style="margin-top:-8px"><strong>COMPOSIÇÃO:</strong> {componentes}</div>
				<div class="posologia" style="margin-top:0px; line-height:5px !important"><strong>POSOLOGIA:</strong> {posologia} </div>
				<div class="doctor" style="line-height:5px !important; margin-top:5px;">
					<p><strong>{doctor}</strong></p><br />
					<p>{crm}</p>
				</div>
				<div class="farmresp" style="line-height:5px !important; margin-top:5px;">
					<strong>FARM. RESP.:</strong>{farmresp}
				</div>
				<div class="flavor">
					<strong>SABOR:</strong> {flavor}
				</div>
			</div>
		</div>
	</div>
</body>


<?php
	class AutoSize{
		public function __toString(){
			return __FILE__;
		}
		public function scalePersona($str){
			$size = strlen($str) - 82;
			$w		= 6.7;
			$h		= 3.5;
		
			if($size <= 80){ 					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			if($size >= 81 && $size <= 110){ 	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 111 && $size <= 140){	$s = 0.8; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 141){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
		public function scaleComponentes($str){
			$size 	= strlen($str) - 82;
			$w		= 5;
			$h		= 4.99;

			if($size <= 500){					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			if($size >= 501 && $size <= 620){	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 621 && $size <= 760){	$s = 0.8;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 761 && $size <= 860){	$s = 0.75;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 861){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}

		public function formatComponentes($str){
			return explode(";", $str)[0] . "; + Componentes";
		}
	}
	
?>