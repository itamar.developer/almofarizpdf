<?php require_once('img/imagens.php'); ?>
<body class="et_p_lipowheat">
	<div class="rotulo">
		<div class="qrcode">
			<img src="{qrcode}"/>
		</div>
        <div class="persona">
			<div class="wrap" style="{scalep}">
				<div class="name"><p>Lipowheat</p></div>
			</div>
			<div class="bottom">				
				<div class="separador"></div>
				<div class="qtdy"><p>{qtdy}</p></div>
			</div>
        </div>
		<?php // echo '<div style="font-size:4px; text-align:center; color:black;">{scalep}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{scale}</div>' ?>
		<div class="infos" style="{scale}">
			<div class="top">
				<div class="separador icon"><img src=<?php echo $icon ?> /></div>
				<div class="componentes"><p><strong>COMPOSIÇÃO: </strong>{componentes}</p></div>
				<div class="posologia"><p><strong>POSOLOGIA: </strong> {posologia}</p></div>
			</div>
	        <div class="bottom">
				<div class="separador icon"><img src=<?php echo $icone_almofariz ?> /></div>			
                <div class="name" style="font-weight:bold"><p>{namereg}</p></div>
				<div class="regreq">
					<span>{reg} <span style="display: inline; margin-left:22px;">{req}</span></span>
				</div>
                <div class="manval">
					<span class="man">{man} </span>
					<span class="val">{val}</span>
				</div>
				<div class="farmresp"><p><b>FARM. RESP.:</b>{farmresp}</span>{matriz}</p></div>	
			</div>
		</div>
	</div>
</body>


<?php
	class AutoSize{
		public function __toString(){
			return __FILE__;
		}
		public function scalePersona($str){
			$size = strlen($str) - 82;
			$w		= 2.11;
			$h		= 1.44;

			// if($size <= 70){ 					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			// if($size >= 71 && $size <= 90){ 	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			// if($size >= 91 && $size <= 120){	$s = 0.8; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			// if($size >= 121){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
		public function scaleComponentes($str){
			$size 	= strlen($str) - 82;
			$w		= 2.65;
			$h		= 2.6;
		
			if($size <= 310){					$s = 1; 	return 'transform: scale('.$s.')'											.'size:'.$size	;}
			if($size >= 311 && $size <= 410){	$s = 0.9; 	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 411 && $size <= 580){	$s = 0.8;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 581 && $size <= 700){	$s = 0.75;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
			if($size >= 701){					$s = 0.7;	return 'transform: scale('.$s.'); width:'.$w/$s.'cm; height:'.$h/$s.'cm'	.'size:'.$size	;}
		}
		
	}
	
?>
