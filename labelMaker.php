<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

date_default_timezone_set('America/Sao_Paulo');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('./class/LabelMaker.php');
require_once('./class/Labels.php');

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    die('<center><strong>Por favor, envie um post com rotulo e data.</strong></center>');
}

$labelMaker = new LabelMaker( LabelMakerApi::getRotuloPost() );

$labelMaker->setData(LabelMakerApi::getDataPost());
echo LabelMakerApi::getPDFPost() ? $labelMaker->outputPDF() : $labelMaker->outputApi();
?>
