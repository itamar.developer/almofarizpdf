<?php
date_default_timezone_set('America/Sao_Paulo');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
require_once('./class/LabelMaker.php');

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    die('<center><strong>Por favor, envie um post com rotulo e data.</strong></center>');
}

$labelMaker = new LabelMaker(LabelMakerApi::getRotuloPost());
$labelMaker->setData(LabelMakerApi::getDataPost());

if(isset($_POST['pdf'])) {
    echo $labelMaker->outputPDF();    
    return;
}
echo $labelMaker->outputHTML();
?>
