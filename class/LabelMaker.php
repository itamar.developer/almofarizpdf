<?php
require_once('./vendor/autoload.php');
require_once('./class/Labels.php');
use Dompdf\Dompdf;
use Dompdf\Options;
use setasign\Fpdi\Fpdi;
use Endroid\QrCode\QrCode;

class LabelMaker{
    public $template = null;
    public $label = null;
    public $style = null;
    public $css = null;
    public $dompdf = null;
    public $exportPath = null;
    public $exportFinalPath = null;
    public $size = null;
    public $data = null;
    public $code = null;

    public static function URL_LAUDO(){
        if(strpos($_SERVER['HTTP_HOST'], 'hml') === 0){
            return "https://hml.farmaciapersonalizada.com.br/checkin/";
        }
        return "https://farmaciapersonalizada.com.br/checkin/";
    }

    public function __construct($label){
        $this->label = Labels::getByName($label);
    
        if(!file_exists($this->getPathTemplatePdf())){
            throw new Exception("PDF template {$this->getPathTemplatePdf()} nao existe, por favor, adicione na pasta PDF");
        }
        
        $this->exportPath = './exports/'.$this->label->pdf.'_'.md5(time().rand()).'.pdf';
        $this->exportFinalPath = 'exports/final/'.$this->label->pdf.'/{name}.pdf';
        $this->size = $this->getPdfDimensions($this->getPathTemplatePdf());
        
        $this->dompdf = new Dompdf();
        $this->template = $this->loadFile("./styles/template.php");
        $this->template = $this->setCss();
    }

    private function handleNameLabel($label){
        return preg_replace('/\\.[^.\\s]{3,4}$/', '', $label);
    }

    public function setData($data){
        $this->data = $this->handledata($data);
    }

    public function isLandscapePortrait(){
        if ($this->size['width'] > $this->size['height']) {
            return 'L';
        } else {
            return 'P';
        }
    }

    public function handleFinalPdf(){
        $this->handlePDF();
        $pdf = new FPDI($this->isLandscapePortrait(),'mm', [$this->size['width'], $this->size['height']]);
        $pdf->AddPage();
        $pdf->setSourceFile($this->getPathTemplatePdf());
        $pdf->useTemplate($pdf->importPage(1), 0, 0, $this->size['width'], $this->size['height']);
        $pdf->setSourceFile($this->exportPath);
        $pdf->useTemplate($pdf->importPage(1), 0, 0, $this->size['width'], $this->size['height']);
        unlink($this->exportPath);
        return $pdf;
    }

    public function outputPDF(){
        $this->handleFinalPdf()->Output();
    }

    public function outputApi(){
        header('Content-Type: application/json');
        $url = dirname("://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]").DIRECTORY_SEPARATOR;
        $url = ($_SERVER['SERVER_NAME'] == 'localhost' ? 'http' : 'https') . $url;
        !file_exists(dirname($this->exportFinalPath)) ? mkdir(dirname($this->exportFinalPath), 0700) : null;
        $fileName = $this->code != null ? str_replace("{name}", $this->code, $this->exportFinalPath) : str_replace("{name}", md5(time().rand()), $this->exportFinalPath);
        $this->handleFinalPdf()->Output($fileName, 'F');
        return LabelMakerApi::returnApi($this->label->name, $url.$fileName.'?v='.time(), $this->data, $this->label->buttons, $this->label->segments);
    }

    public function handlePDF(){
        $this->dompdf->setPaper([0, 0, $this->cmToPx($this->size['width']), $this->cmToPx($this->size['height'])]);
        $this->dompdf->set_option('defaultMediaType', 'all');
        $this->dompdf->set_option('isFontSubsettingEnabled', true);
        $this->dompdf->loadHtml($this->outputHTML(), 'UTF-8');
        $this->dompdf->render();
        file_put_contents($this->exportPath, $this->dompdf->output());
    }

    private function handleStylePath(){
        return "./styles/".$this->label->style.".php";
    }

    public function outputHTML($minify = true){
        if($this->data == null){
            throw new Exception('Por favor, defina ->setData()');
        }
        $labelHtml = $this->loadFile($this->handleStylePath());
        $labelHtml = $this->populateHtml($labelHtml);
        return $minify ? $this->minify($labelHtml) : $labelHtml;
    }

    private function populateHtml($labelHtml){
        $labelHtml = str_replace("{template}", $labelHtml, $this->template);
        $autoSize = new AutoSize();
        
        /**
         * Essa class é instanciada a partir do arquivo importando .php
         * Exemplo r160.php dentro do arquivo tem a class AutoSize
         */
        try{
            foreach($this->data as $key => $data) {
                if($key == 'posologia'){
                    $data = mb_strtoupper($data, mb_internal_encoding());
                }
                if($key == 'componentes'){
                    if(array_search('formatComponentes', get_class_methods($autoSize))){
                        $data = $autoSize->formatComponentes(mb_strtoupper($data, mb_internal_encoding()));
                    }else{
                        $data = mb_strtoupper($data, mb_internal_encoding());
                    }
                }
                if($key == 'posologiaDesc'){
                    if(array_search('limitCaracterPosologia', get_class_methods($autoSize))){
                        $data = substr($data, 0, $autoSize->limitCaracterPosologia());
                    }
                }

                $labelHtml = str_replace("{".$key."}", $data, $labelHtml);
            }
            
            if(array_search('scaleComponentes', get_class_methods($autoSize))){
                $labelHtml = str_replace("{scale}", $autoSize->scaleComponentes($data.$this->data->componentes.$this->data->posologia.$this->data->farmresp.$this->data->namereg.$this->data->doctor.$this->data->ativosFormula), $labelHtml);
            }    

            if(array_search('scalePersona', get_class_methods($autoSize))){
                $labelHtml = str_replace("{scalep}", $autoSize->scalePersona($data.$this->data->tituloFormula.$this->data->name.$this->data->posologiaDesc), $labelHtml);
            }

            if(array_search('scalePosologia', get_class_methods($autoSize))){
                if(array_search('limitCaracterPosologia', get_class_methods($autoSize))){
                    $this->data->posologiaDesc = substr($this->data->posologiaDesc, 0, $autoSize->limitCaracterPosologia());
                }
                $labelHtml = str_replace("{scaleposologia}", $autoSize->scalePosologia($this->data->posologiaDesc), $labelHtml);
            }
            
        }catch(Error $e){}
        
        //Atributos
        $labelHtml = str_replace("{titulo_formula}", $this->data->tituloFormula, $labelHtml);
        $labelHtml = str_replace("{descr_produto}", $this->data->descrProduto, $labelHtml);
        $labelHtml = str_replace("{ativos_formula}", $this->data->ativosFormula, $labelHtml);
        $labelHtml = str_replace("{qrcode}", $this->handleQrCode()->writeDataUri(), $labelHtml);
        return $labelHtml;
    }

    private function handleStylePathCss(){
        if(file_exists("./styles/style/".$this->label->css.".css")){
            return "./styles/style/".$this->label->css.".css";
        }
        return false;
    }

    private function handleStylePathCssExtra($name){
        return "./styles/style/".$name.".css";
    }

    private function setCss(){
        $css = '';
        if($this->handleStylePathCss()){
            $css = $this->loadFile($this->handleStylePathCss());
        }

        if(isset($this->label->extraCss)){
            if(is_array($this->label->extraCss)){
                foreach($this->label->extraCss as $extraCss){
                    if($this->handleStylePathCssExtra($extraCss)){
                        $css .= $this->loadFile($this->handleStylePathCssExtra($extraCss));
                    }
                }
            }else{
                $css .= $this->loadFile($this->handleStylePathCssExtra($this->label->extraCss));
            }
            
        }
        return str_replace("{css}", $css, $this->template);
    }

    private function loadFile($file){
        ob_start();
        include $file;
        $file = ob_get_clean();
        return $file;
    }

    private function handledata($data){
        $json = json_decode($data);
        $this->code = $json->code;
        return $json;
    }

    private function handleQrCode(){
        $qrCode = new QrCode(LabelMaker::URL_LAUDO() . $this->data->code);
        $qrCode->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 127]);
        $qrCode->setSize(1000);
        return $qrCode;
    }

    private function cmToPx($cm){
        return round($cm * 2.835);
    }

    private static function pxToCm($px){
        return round($px / 2.835);
    }

    private function getPdfDimensions($path, $box="MediaBox") {
        $stream = new SplFileObject($path); 
        $result = false;
        while (!$stream->eof()) {
            if (preg_match("/".$box."\[[0-9]{1,}.[0-9]{1,} [0-9]{1,}.[0-9]{1,} ([0-9]{1,}.[0-9]{1,}) ([0-9]{1,}.[0-9]{1,})\]/", $stream->fgets(), $matches)) {
                if($this->label->pdf == 'et-sache') {
                    $result["width"] = 45;
                    $result["height"] = 95; 
                } else {
                    $result["width"] = $this->pxToCm($matches[1]);
                    $result["height"] = $this->pxToCm($matches[2]); 
                }
                break;
            }
        }
        return $result;
    }

    private function getPathTemplatePdf(){
        return "./pdf/".$this->label->pdf.".pdf";
    }

    private function minify($buffer) {
        return preg_replace(
            array('/ {2,}/', '/<!--.*?-->|\t|(?:\r?\n[ \t]*)+/s'),
            array(' ', ''), $buffer
        );
    }
}

class LabelMakerApi{
    public static function getDataPost(){
        if(isset($_POST['data'])){
            return $_POST['data'];
        }
    }

    public static function getRotuloPost(){
        if(isset($_POST['string_rotulo']) && $_POST['string_rotulo'] != ''){
            return $_POST['string_rotulo'];
        }

        if(isset($_POST['rotulo'])){
            return $_POST['rotulo'];
        }
    }

    public static function getPDFPost(){
        if(isset($_POST['pdf'])){
            return $_POST['pdf'];
        }
    }

    public static function returnApi($label, $pathFile, $data, $buttons = null, $segments = null){
        return json_encode([
            'rotulo' => $label,
            'file' => $pathFile,
            'data' => $data,
            'buttons' => $buttons,
            'segments' => $segments,
        ]);
    }
}