<?php

class Labels{
    static public $labels = [];
    public $name;
    public $pdf;
    public $style;
    public $css;
    public $buttons;
    public $segments;

    public static function getByName($name){
        $labelsObj = new Labels();
        Labels::$labels = Labels::getLabels();
        $labelsObj->name = $name;
        $labelsObj->pdf = Labels::$labels[$name]['pdf'];
        $labelsObj->style = Labels::$labels[$name]['style'];
        $labelsObj->css = isset(Labels::$labels[$name]['css']) ? Labels::$labels[$name]['css'] : Labels::$labels[$name]['style'];
        $labelsObj->extraCss = isset(Labels::$labels[$name]['extraCss']) ? Labels::$labels[$name]['extraCss'] : null;
        $labelsObj->buttons  = isset(Labels::$labels[$name]['buttons']) ? Labels::$labels[$name]['buttons'] : null;
        $labelsObj->segments  = isset(Labels::$labels[$name]['segments']) ? Labels::$labels[$name]['segments'] : null;

        return $labelsObj;
    }

    public static function getLabels() {
        $labels = [];
        foreach(glob(__DIR__ . "/labels/*.php") as $key => $file) {
            $labels = $labels + include $file;
        }

        ksort($labels);
        return $labels;
    }
}