<?php 
return [
    'pump-gr-medicamentos' => [
        'pdf' => 'pump-gr-medicamentos',
        'style' => 'pump_g',
        'buttons' => $buttonR80Medicamento = [
            [
                'name' => 'Original',
                'goTo' => 'pump-gr-medicamentos'
            ],
            [
                'name' => 'Tarja preta',
                'goTo' => 'pump-gr-medicamentos-tarja-preta'
            ],
            [
                'name' => 'Tarja vermelha 01',
                'goTo'  => 'pump-gr-medicamentos-tarja-vermelha-01',
            ],
            [
                'name' => 'Tarja vermelha 02',
                'goTo'  => 'pump-gr-medicamentos-tarja-vermelha-02',
            ],
            [
                'name' => 'Tarja vermelha 03',
                'goTo'  => 'pump-gr-medicamentos-tarja-vermelha-03',
            ],
        ]
    ],
    'pump-gr-medicamentos-tarja-preta' => [
        'pdf' => 'pump-gr-medicamentos-tarja-preta',
        'style' => 'pump_g',
        'extraCss' => 'pump_g_tarja',
        'buttons' => $buttonR80Medicamento
    ],
    'pump-gr-medicamentos-tarja-vermelha-01' => [
        'pdf' => 'pump-gr-medicamentos-tarja-vermelha-01',
        'style' => 'pump_g',
        'extraCss' => 'pump_g_tarja',
        'buttons' => $buttonR80Medicamento
    ],
    'pump-gr-medicamentos-tarja-vermelha-02' => [
        'pdf' => 'pump-gr-medicamentos-tarja-vermelha-02',
        'style' => 'pump_g',
        'extraCss' => 'pump_g_tarja',
        'buttons' => $buttonR80Medicamento
    ],
    'pump-gr-medicamentos-tarja-vermelha-03' => [
        'pdf' => 'pump-gr-medicamentos-tarja-vermelha-03',
        'style' => 'pump_g',
        'extraCss' => 'pump_g_tarja',
        'buttons' => $buttonR80Medicamento
    ],
    'pump-gr-dermocosmeticos' => [
        'pdf' => 'pump-gr-dermocosmeticos',
        'style' => 'pump_g',
    ],
    'pump-gr-nutraceuticos' => [
        'pdf' => 'pump-gr-nutraceuticos',
        'style' => 'pump_g',
    ],
    //PARAMETRO COSMGR//
    'pump-gr-creme-maos' => [
        'pdf' => 'pump-gr-creme-maos',
        'style' => 'pump_g_creme',
    ],
    'alm_cosmgr-epumpmg' => [
        'pdf' => 'pump-gr-creme-maos',
        'style' => 'pump_g_creme',
    ],

    'cosmgr' => [
        'pdf' => 'pump-gr-creme-maos',
        'style' => 'pump_g_creme',
    ],
    
];