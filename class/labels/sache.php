<?php 
return [
    'sache_70' => [
        'pdf' => 'sache_70',
        'style' => 'sache_70',
    ],
    'sache_140' => [
        'pdf' => 'sache_140',
        'style' => 'sache_140',
    ],

    'sach70' => [
        'pdf' => 'sache_70',
        'style' => 'sache_70',
    ],
    'sach140' => [
        'pdf' => 'sache_140',
        'style' => 'sache_140',
    ],

    'sc70' => [
        'pdf' => 'sache_70',
        'style' => 'sache_70',
        'buttons' => $scButton = [
            [
                'name' => 'Rotulo Caixa',
                'goTo' => 'et-sache'
            ],
            [
                'name' => 'Rotulo Sache 70',
                'goTo' => 'sc70'
            ],
            [
                'name' => 'Rotulo Sache 140',
                'goTo'  => 'sc140',
            ],
        ]
    ],
    'sc140' => [
        'pdf' => 'sache_140',
        'style' => 'sache_140',
        'buttons' => $scButton
    ],
];
