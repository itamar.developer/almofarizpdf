<?php 
return [
    /**
     * ALM
     * R80
     */
    'alm_r80-fitnes' => [
        'pdf' => 'alm_r80-fitnes',
        'style' => 'alm_r80',
    ],
    'alm_r80-imuni' => [
        'pdf' => 'alm_r80-imuni',
        'style' => 'alm_r80',
    ],
    'alm_r80-peso' => [
        'pdf' => 'alm_r80-peso',
        'style' => 'alm_r80',
    ],
    'alm_r80-beleza' => [
        'pdf' => 'alm_r80-beleza',
        'style' => 'alm_r80',
    ],
    'alm_r80-estar' => [
        'pdf' => 'alm_r80-estar',
        'style' => 'alm_r80',
    ],
    /**
     * ALM
     * R160
     */
    'alm_r160-fitnes' => [
        'pdf' => 'alm_r160-fitnes',
        'style' => 'alm_r160',
    ],
    'alm_r160-imuni' => [
        'pdf' => 'alm_r160-imuni',
        'style' => 'alm_r160',
    ],
    'alm_r160-peso' => [
        'pdf' => 'alm_r160-peso',
        'style' => 'alm_r160',
    ],
    'alm_r160-beleza' => [
        'pdf' => 'alm_r160-beleza',
        'style' => 'alm_r160',
    ],
    'alm_r160-estar' => [
        'pdf' => 'alm_r160-estar',
        'style' => 'alm_r160',
    ],
    /**
     * ALM
     * R320
     */
    'alm_r320-fitnes' => [
        'pdf' => 'alm_r320-fitnes', 
        'style' => 'alm_r320',
    ],
    'alm_r320-peso' => [
        'pdf' => 'alm_r320-peso', 
        'style' => 'alm_r320',
    ],
    'alm_r320-beleza' => [
        'pdf' => 'alm_r320-beleza', 
        'style' => 'alm_r320',
    ],
];