<?php 
return [
    'alm_sh-cnd-sh-rest' => [
        'pdf' => 'alm_sh-cnd-sh-rest',
        'style' => 'alm_shampoo_rest',
        'extraCss' => ['alm_shampoo', 'alm_shampoo_rest'],
    ],
    'alm_sh-cnd-cnd-rest' => [
        'pdf' => 'alm_sh-cnd-cnd-rest',
        'style' => 'alm_condicionador_rest',
        'extraCss' => ['alm_shampoo']
    ],
    'alm_mascara-msk-rest' => [
        'pdf' => 'mascara',
        'style' => 'alm_mascara',
    ],
    'mascara' => [
        'pdf' => 'eco_mascara',
        'style' => 'alm_mascara',
    ],
    'alm_leavin-lv-rest' => [
        'pdf' => 'alm_leavin-lv-rest',
        'style' => 'alm_sh_leavin_rest',
        'extraCss' => ['alm_shampoo', 'alm_shampoo_rest'],

    ],
];