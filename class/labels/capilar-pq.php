<?php 
return [
    'leavin' => [
        'pdf' => 'sh-cnd-leavin-pq',
        'style' => 'leavin',
        'extraCss' => ['tonico'],  
        'buttons' => $buttonR80Mate150ml = [
            [
                'name' => 'Original até 150ml',
                'goTo'  => 'sh-cdn-original-pq',
            ],
            [
                'name' => 'Shampoo até 150ml',
                'goTo'  => 'sh-cdn-shampoo-pq',
            ],
            [
                'name' => 'Condicionador até 150ml',
                'goTo'  => 'sh-cdn-condicionador-pq',
            ],
            [
                'name' => 'Leavin até 150ml',
                'goTo'  => 'leavin_pq',
            ],
            [
                'name' => 'Tonico até 150ml',
                'goTo'  => 'tonico_pq',
            ],
            [
                'name' => 'Sabonete até 150ml',
                'goTo'  => 'sh-cdn-sabonete-liquido-pq',
            ],
            [
                'name' => 'Creme até 150ml',
                'goTo'  => 'sh-cnd-creme-corporal-pq',
            ],
        ],
    ],

    'sh-cdn-original-pq' => [
        'pdf' => 'sh-cdn-original-pq',
        'style' => 'alm_leavin',
        'extraCss' => ['fix_alm_leavin'],
        'buttons' => $buttonR80Mate150ml
    ],

    'leavin_pq' => [
        'pdf' => 'sh-cnd-leavin-pq',
        'style' => 'alm_leavin',
        'extraCss' => ['fix_alm_leavin'],
        'buttons' => $buttonR80Mate150ml
    ],

    'sh-cdn-sabonete-liquido-pq' => [
        'pdf' => 'sh-cdn-sabonete-liquido-pq',
        'style' => 'alm_leavin',
        'extraCss' => ['fix_alm_leavin'],
        'buttons' => $buttonR80Mate150ml
    ],

    'sh-cnd-creme-corporal-pq' => [
        'pdf' => 'sh-cnd-creme-corporal-pq',
        'style' => 'alm_leavin',
        'extraCss' => ['fix_alm_leavin'],
        'buttons' => $buttonR80Mate150ml
    ],

    'tonico_pq' => [
        'pdf' => 'sh-cnd-tonico-pq',
        'style' => 'alm_leavin',
        'extraCss' => ['tonico', 'fix_alm_leavin'],
        'buttons' => $buttonR80Mate150ml
    ],

    'sh-cdn-shampoo-pq' => [
        'pdf' => 'sh-cdn-shampoo-pq',
        'style' => 'alm_leavin',
        'extraCss' => ['fix_alm_leavin'],
        'buttons' => $buttonR80Mate150ml
    ],

    'sh-cdn-condicionador-pq' => [
        'pdf' => 'sh-cdn-condicionador-pq',
        'style' => 'alm_leavin',
        'extraCss' => ['sh-cnd-condicionador.pq'],
        'buttons' => $buttonR80Mate150ml
    ],
];