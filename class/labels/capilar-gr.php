<?php 
return [
//PARAMETRO LEAVIN-GR//
    'leavin-gr' => [
        'pdf' => 'sh-cnd-leavin-gr',
        'style' => 'leavin',
        'extraCss' => ['tonico'],  
        'buttons' => $buttonR80Mate250ml = [
            [
                'name' => 'original até 250ml',
                'goTo'  => 'leavin-original-gr',
            ],
            [
                'name' => 'Leavin até 250ml',
                'goTo'  => 'sh-cnd-leavin-gr',
            ],
            [
                'name' => 'Tonico até 250ml',
                'goTo'  => 'sh-cnd-tonico-gr',
            ],
            [
                'name' => 'Sabonete até 250ml',
                'goTo'  => 'sh-cdn-sabonete-liquido-gr',
            ],
            [
                'name' => 'Creme até 250ml',
                'goTo'  => 'sh-cnd-creme-corporal-gr',
            ],
        ],
    ],

    
    'sh-cond' => [
        'pdf' => 'sh-cnd-original-gr',
        'style' => 'sh-cnd',
        'extraCss' => ['r160', 'sh-cnd-condicionador-gr'],
        'buttons' => $buttonR80Mate250ml= [
            [
                'name' => 'Original até 250ml',
                'goTo'  => 'sh-cond',
            ],
            [
                'name' => 'Shampoo até 250ml',
                'goTo'  => 'sh-cnd-shampoo-gr',    
            ],
            [
                'name' => 'Condicionador até 250ml',
                'goTo'  => 'sh-cnd-condicionador-gr',
            ],
            [
                'name' => 'Leavin até 250ml',
                'goTo'  => 'sh-cnd-leavin-gr',
            ],
            [
                'name' => 'Tonico até 250ml',
                'goTo'  => 'sh-cnd-tonico-gr',
            ],
            [
                'name' => 'Sabonete até 250ml',
                'goTo'  => 'sh-cnd-sabonete-liquido-gr',
            ],
            [
                'name' => 'Creme até 250ml',
                'goTo'  => 'sh-cnd-creme-corporal-gr',
            ],
        ],
    ],

    'sh-cnd' => [
        'pdf' => 'sh-cnd-original-gr',
        'style' => 'sh-cnd',
        'extraCss' => ['r160', 'sh-cnd-condicionador-gr'],
        'buttons' => $buttonR80Mate250ml,
    ],

    'sh-cnd-shampoo-gr' => [
        'pdf' => 'sh-cnd-shampoo-gr',
        'style' => 'sh-cnd',
        'extraCss' => ['r160', 'sh-cnd-shampoo-gr'],
        'buttons' => $buttonR80Mate250ml
    ],

    'sh-cnd-condicionador-gr' => [
        'pdf' => 'sh-cnd-condicionador-gr',
        'style' => 'sh-cnd',
        'extraCss' => ['r160', 'sh-cnd-condicionador-gr'],
        'buttons' => $buttonR80Mate250ml
    ],


    'sh-cnd-leavin-gr' => [
        'pdf' => 'sh-cnd-leavin-gr',
        'style' => 'sh-cnd',
        'extraCss' => ['r160', 'sh-cnd-leavin-gr'],
        'buttons' => $buttonR80Mate250ml
    ],

    'sh-cnd-tonico-gr' => [
        'pdf' => 'sh-cnd-tonico-gr',
        'style' => 'sh-cnd',
        'extraCss' => ['r160', 'sh-cnd-tonico-gr'],
        'buttons' => $buttonR80Mate250ml

    ],

    'sh-cnd-sabonete-liquido-gr' => [
        'pdf' => 'sh-cnd-sabonete-liquido-gr',
        'style' => 'sh-cnd',
        'extraCss' => ['r160', 'sh-cnd-sabonete-liquido-gr'],
        'buttons' => $buttonR80Mate250ml

    ],

    'sh-cnd-creme-corporal-gr' => [
        'pdf' => 'sh-cnd-creme-corporal-gr',
        'style' => 'sh-cnd',
        'extraCss' => ['r160', 'sh-cnd-creme-corporal-gr'],
        'buttons' => $buttonR80Mate250ml
    ],
];



