<?php 
return [
    'alm_sh-cnd-sh-ant' => [
        'pdf' => 'alm_sh-cnd-sh-ant',
        'style' => 'alm_sh',
    ],
    'alm_sh-cnd-cnd-ant' => [
        'pdf' => 'alm_sh-cnd-cnd-ant',
        'style' => 'alm_condicionador',
        'extraCss' => ['alm_shampoo']
    ],
    // 'alm_leavin-lv-ant' => [
    //     'pdf' => 'alm_leavin-lv-ant',
    //     'style' => 'alm_sh_leavin_ant',
    //     'extraCss' => ['alm_shampoo', 'alm_shampoo_ant'],
    // ],
];