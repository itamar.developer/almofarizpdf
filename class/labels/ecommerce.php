<?php 
return [
  'alm_sh-cond-sh-bri' => [
    'pdf' => 'eco_shampoo_gr',
    'style' => 'alm_shampoo',
    'extraCss' => 'alm_shampoo_eco', 
  ],

  'alm_sh-cond-cnd-bri' => [
    'pdf' => 'eco_condicionador_gr',
    'style' => 'alm_shampoo',
    'extraCss' => 'alm_shampoo_eco',
  ],
  
  'alm_sh-cond-eccreg' => [
    'pdf' => 'eco_creme_corporal_gr',
    'style' => 'alm_shampoo',
    'extraCss' => 'alm_shampoo_eco',
  ],

  'alm_sh-cnd-ecshpq' => [
    'pdf' => 'eco_shampoo_pq',
    'style' => 'alm_leavin_eco',
  ],

  'alm_leavin-ecshpq' => [
    'pdf' => 'eco_shampoo_pq',
    'style' => 'alm_leavin_eco',
  ],

  'eco_condicionador_pq' => [
    'pdf' => 'eco_condicionador_pq',
    'style' => 'alm_leavin_eco',
  ],

  'alm_leavin-econdpq' => [
    'pdf' => 'eco_condicionador_pq',
    'style' => 'alm_leavin_eco',
  ],

  'alm_sh-cond-pq-lv-ant' => [
    'pdf' => 'eco_tonico',
    'style' => 'alm_leavin_eco',
  ],

  'alm_leavin-lv-ant' => [
    'pdf' => 'eco_tonico',
    'style' => 'alm_leavin_eco',
  ],

  'alm_leavin-lv-bri' => [
    'pdf' => 'eco_leavin',
    'style' => 'alm_leavin_eco',
  ],

  'alm_mascara-msk-bri' => [
    'pdf' => 'eco_mascara',
    'style' => 'alm_mascara',
  ],

  'alm_cosmpq-epumpmp' => [
    'pdf' => 'eco_pump_maos_pq',
    'style' => 'pump_p_creme',
  ],

  'alm_cosmpq-epumpfp' => [
    'pdf' => 'eco_pump_facial_pq',
    'style' => 'pump_p_creme',
  ],

  'epumpmg' => [
    'pdf' => 'eco_pump_maos_gr',
    'style' => 'pump_g_creme',
  ],
];