<?php 
return [
    //R80
    'r80' => [
        'pdf' => 'r80-medicamentos',
        'style' => 'r80',
        'extraCss' => ['r80'],
        'segments' => $segmentsR80Medicamento = [
            [
                'name' => 'Medicamentos',
                'goTo' => 'r80-medicamentos',
                'color' => 'blue'
            ],
            [
                'name' => 'Nutracêuticos',
                'goTo'  => 'r80-nutraceuticos',
                'color' => 'green'
            ],
            [
                'name' => 'Dermocosméticos',
                'goTo'  => 'r80-dermocosmeticos',
                'color' => 'purple'
            ],
        ],
        'buttons' => $buttonR80Medicamento = [
            [
                'name' => 'Original',
                'goTo' => 'r80-medicamentos'
            ],
            [
                'name' => 'Tarja preta',
                'goTo' => 'r80-medicamentos-tarja-preta'
            ],
            [
                'name' => 'Tarja vermelha 01',
                'goTo'  => 'r80-medicamentos-tarja-vermelho-01',
            ],
            [
                'name' => 'Tarja vermelha 02',
                'goTo'  => 'r80-medicamentos-tarja-vermelho-02',
            ],
        ]
    ],

    'r80-dermocosmeticos' => [
        'pdf' => 'r80-dermocosmeticos',
        'style' => 'r80',
        'extraCss' => ['r80'],
        'segments' => $segmentsR80Medicamento
    ],
    'r80-nutraceuticos' => [
        'pdf' => 'r80-nutraceuticos',
        'style' => 'r80',
        'extraCss' => ['r80'],
        'segments' => $segmentsR80Medicamento
    ],
    'r80-medicamentos' => [
        'pdf' => 'r80-medicamentos',
        'style' => 'r80',
        'extraCss' => ['r80'],
        'buttons' => $buttonR80Medicamento,
        'segments' => $segmentsR80Medicamento
    ],
    'r80-medicamentos-tarja-preta' => [
        'pdf' => 'r80-medicamentos-tarja-preta',
        'style' => 'r80',
        'extraCss' => ['r80', 'r80_tarja'],
        'buttons' => $buttonR80Medicamento,
        'segments' => $segmentsR80Medicamento
    ],
    'r80-medicamentos-tarja-vermelho-01' => [
        'pdf' => 'r80-medicamentos-tarja-vermelha-01',
        'style' => 'r80',
        'extraCss' => ['r80', 'r80_tarja'],
        'buttons' => $buttonR80Medicamento,
        'segments' => $segmentsR80Medicamento
    ],
    'r80-medicamentos-tarja-vermelho-02' => [
        'pdf' => 'r80-medicamentos-tarja-vermelha-02',
        'style' => 'r80',
        'extraCss' => ['r80', 'r80_tarja'],
        'buttons' => $buttonR80Medicamento,
        'segments' => $segmentsR80Medicamento
    ],



    //R160
    'r160' => [
        'pdf' => 'r160-medicamentos',
        'style' => 'r160',
        'extraCss' => ['r160'],
        'segments' => $segmentsR160Medicamento = [
            [
                'name' => 'Medicamentos',
                'goTo' => 'r160-medicamentos',
                'color' => 'blue'
            ],
            [
                'name' => 'Nutracêuticos',
                'goTo'  => 'r160-nutraceuticos',
                'color' => 'green'
            ],
            [
                'name' => 'Dermocosméticos',
                'goTo'  => 'r160-dermocosmeticos',
                'color' => 'purple'
            ],
        ],
        'buttons' => $buttonR160Medicamento = [
            [
                'name' => 'Original',
                'goTo' => 'r160-medicamentos'
            ],
            [
                'name' => 'Tarja preta 01',
                'goTo'  => 'r160-medicamentos-tarja-preta',
            ],
            [
                'name' => 'Tarja vermelha 01',
                'goTo'  => 'r160-medicamentos-tarja-vermelho-01',
            ],
            [
                'name' => 'Tarja vermelha 02',
                'goTo'  => 'r160-medicamentos-tarja-vermelho-02',
            ],
        ],
    ],

    'r160-medicamentos' => [
        'pdf' => 'r160-medicamentos',
        'style' => 'r160',
        'extraCss' => ['r160', 'r160_tarja'],
        'segments' => $segmentsR160Medicamento,
        'buttons' => $buttonR160Medicamento
    ],


    'r160-dermocosmeticos' => [
        'pdf' => 'r160-dermocosmeticos',
        'style' => 'r160',
        'extraCss' => ['r160', 'r160_tarja'],
        'segments' => $segmentsR160Medicamento
    ],
    
    'r160-nutraceuticos' => [
        'pdf' => 'r160-nutraceuticos',
        'style' => 'r160',
        'extraCss' => ['r160', 'r160_tarja'],
        'segments' => $segmentsR160Medicamento
    ],
    'r160-medicamentos-tarja-preta' => [
        'pdf' => 'r160-medicamentos-tarja-preta',
        'style' => 'r160',
        'extraCss' => ['r160', 'r160_tarja'],
        'buttons' => $buttonR160Medicamento,
        'segments' => $segmentsR160Medicamento
    ],
    'r160-medicamentos-tarja-vermelho-01' => [
        'pdf' => 'r160-medicamentos-tarja-vermelha-01',
        'style' => 'r160',
        'extraCss' => ['r160', 'r160_tarja'],
        'buttons' => $buttonR160Medicamento,
        'segments' => $segmentsR160Medicamento
    ],
    'r160-medicamentos-tarja-vermelho-02' => [
        'pdf' => 'r160-medicamentos-tarja-vermelha-02',
        'style' => 'r160',
        'extraCss' => ['r160', 'r160_tarja'],
        'buttons' => $buttonR160Medicamento,
        'segments' => $segmentsR160Medicamento
    ],

    //R320
    'r320' => [
        'pdf' => 'r320-medicamentos',
        'style' => 'r320',
        'extraCss' => ['r320', 'r320_tarja'],
        'segments' => $segmentsR320Medicamento = [
            [
                'name' => 'Medicamentos',
                'goTo' => 'r320-medicamentos',
                'color' => 'blue'
            ],
            [
                'name' => 'Nutracêuticos',
                'goTo'  => 'r320-nutraceuticos',
                'color' => 'green'
            ],
            [
                'name' => 'Dermocosméticos',
                'goTo'  => 'r320-dermocosmeticos',
                'color' => 'purple'
            ],
        ],
        'buttons' => $buttonR320Medicamento = [
            [
                'name' => 'Original',
                'goTo' => 'r320-medicamentos'
            ],
            [
                'name' => 'Tarja preta',
                'goTo' => 'r320-medicamentos-tarja-preta'
            ],
            [
                'name' => 'Tarja vermelha 01',
                'goTo'  => 'r320-medicamentos-tarja-vermelho-01',
            ],
            [
                'name' => 'Tarja vermelha 02',
                'goTo'  => 'r320-medicamentos-tarja-vermelho-02',
            ],
        ]
    ],

    'r320-medicamentos' => [
        'pdf' => 'r320-medicamentos',
        'style' => 'r320',
        'extraCss' => ['r320'],
        'buttons' => $buttonR320Medicamento,
        'segments' => $segmentsR320Medicamento,
    ],

    'r320-dermocosmeticos' => [
        'pdf' => 'r320-dermocosmeticos',
        'style' => 'r320',
        'extraCss' => ['r320'],
        'segments' => $segmentsR320Medicamento,
    ],
    'r320-nutraceuticos' => [
        'pdf' => 'r320-nutraceuticos',
        'style' => 'r320',
        'extraCss' => ['r320'],
        'segments' => $segmentsR320Medicamento,
    ],
    
    'r320-medicamentos-tarja-preta' => [
        'pdf' => 'r320-medicamentos-tarja-preta',
        'style' => 'r320',
        'extraCss' => ['r320', 'r320_tarja'],
        'buttons' => $buttonR320Medicamento,
        'segments' => $segmentsR320Medicamento
    ],
    'r320-medicamentos-tarja-vermelho-01' => [
        'pdf' => 'r320-medicamentos-tarja-vermelha-01',
        'style' => 'r320',
        'extraCss' => ['r320', 'r320_tarja'],
        'buttons' => $buttonR320Medicamento,
        'segments' => $segmentsR320Medicamento
    ],
    'r320-medicamentos-tarja-vermelho-02' => [
        'pdf' => 'r320-medicamentos-tarja-vermelha-02',
        'style' => 'r320',
        'extraCss' => ['r320', 'r320_tarja'],
        'buttons' => $buttonR320Medicamento,
        'segments' => $segmentsR320Medicamento
    ],




    //R500
    'r500' => [
        'pdf' => 'r500-medicamentos',
        'style' => 'r500',
        'extraCss' => ['r500'],
        'segments' => $segmentsR500Medicamento = [
            [
                'name' => 'Medicamentos',
                'goTo' => 'r500-medicamentos',
                'color' => 'blue'
            ],
            [
                'name' => 'Nutracêuticos',
                'goTo'  => 'r500-nutraceuticos',
                'color' => 'green'
            ],
            [
                'name' => 'Dermocosméticos',
                'goTo'  => 'r500-dermocosmeticos',
                'color' => 'purple'
            ],
        ],
        'buttons' => $buttonR500Medicamento = [
            [
                'name' => 'Original',
                'goTo' => 'r500-medicamentos'
            ],
            [
                'name' => 'Tarja preta',
                'goTo' => 'r500-medicamentos-tarja-preta'
            ],
            [
                'name' => 'Tarja vermelha 01',
                'goTo'  => 'r500-medicamentos-tarja-vermelho-01',
            ],
            [
                'name' => 'Tarja vermelha 02',
                'goTo'  => 'r500-medicamentos-tarja-vermelho-02',
            ],
        ]
    ],


    'r500-dermocosmeticos' => [
        'pdf' => 'r500-dermocosmeticos',
        'style' => 'r500',
        'extraCss' => ['r500'],
        'segments' => $segmentsR500Medicamento,
    ],
    'r500-nutraceuticos' => [
        'pdf' => 'r500-nutraceuticos',
        'style' => 'r500',
        'extraCss' => ['r500'],
        'segments' => $segmentsR500Medicamento,
    ],
    
    'r500-medicamentos' => [
        'pdf' => 'r500-medicamentos',
        'style' => 'r500',
        'extraCss' => ['r500'],
        'segments' => $segmentsR500Medicamento,
        'buttons' => $buttonR500Medicamento
    ],
    'r500-medicamentos-tarja-preta' => [
        'pdf' => 'r500-medicamentos-tarja-preta',
        'style' => 'r500',
        'extraCss' => ['r500'],
        'buttons' => $buttonR500Medicamento,
        'segments' => $segmentsR500Medicamento
    ],
    'r500-medicamentos-tarja-vermelho-01' => [
        'pdf' => 'r500-medicamentos-tarja-vermelha-01',
        'style' => 'r500',
        'extraCss' => ['r500'],
        'buttons' => $buttonR500Medicamento,
        'segments' => $segmentsR500Medicamento
    ],
    'r500-medicamentos-tarja-vermelho-02' => [
        'pdf' => 'r500-medicamentos-tarja-vermelha-02',
        'style' => 'r500',
        'extraCss' => ['r500'],
        'buttons' => $buttonR500Medicamento,
        'segments' => $segmentsR500Medicamento
    ],
];