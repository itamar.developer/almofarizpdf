<?php 
return [
    'etgr' => [
        'pdf' => 'etgr-medicamentos',
        'style' => 'et_g',
        'segments' => $segmentsETGR = [
            [
                'name' => 'Medicamentos',
                'goTo' => 'etgr-medicamentos',
                'color' => 'blue'
            ],
            [
                'name' => 'Nutracêuticos',
                'goTo'  => 'etgr-nutraceuticos',
                'color' => 'green'
            ],
            [
                'name' => 'Dermocosméticos',
                'goTo'  => 'etgr-dermocosmeticos',
                'color' => 'purple'
            ],
        ],
    ],

    'etgr-dermocosmeticos' => [
        'pdf' => 'etgr-dermocosmeticos',
        'style' => 'et_g',
        'segments' => $segmentsETGR
    ],
    'etgr-medicamentos' => [
        'pdf' => 'etgr-medicamentos',
        'style' => 'et_g',
        'segments' => $segmentsETGR
    ],
    'etgr-nutraceuticos' => [
        'pdf' => 'etgr-nutraceuticos',
        'style' => 'et_g',
        'segments' => $segmentsETGR
    ],



    'etpq' => [
        'pdf' => 'etpq-medicamentos',
        'style' => 'et_p',
        'segments' => $segmentsETPQ = [
            [
                'name' => 'Medicamentos',
                'goTo' => 'etpq-medicamentos',
                'color' => 'blue'
            ],
            [
                'name' => 'Nutracêuticos',
                'goTo'  => 'etpq-nutraceuticos',
                'color' => 'green'
            ],
            [
                'name' => 'Dermocosméticos',
                'goTo'  => 'etpq-dermocosmeticos',
                'color' => 'purple'
            ],
        ],
    ],
    'etpq-dermocosmeticos' => [
        'pdf' => 'etpq-dermocosmeticos',
        'style' => 'et_p',
        'segments' => $segmentsETPQ
    ],
    'etpq-medicamentos' => [
        'pdf' => 'etpq-medicamentos',
        'style' => 'et_p',
        'segments' => $segmentsETPQ
    ],
    'etpq-nutraceuticos' => [
        'pdf' => 'etpq-nutraceuticos',
        'style' => 'et_p',
        'segments' => $segmentsETPQ
    ],


    'alm_etpq-beleza' => [
        'pdf' => 'et_p_lipowheat',
        'style' => 'et_p_lipowheat',
    ],
    'lipow' => [
        'pdf' => 'et_p_lipowheat',
        'style' => 'et_p_lipowheat',
    ],
    'alm_lipow-beleza' => [
        'pdf' => 'et_p_lipowheat',
        'style' => 'et_p_lipowheat',
    ],
    
];