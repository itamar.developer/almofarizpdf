<?php 
return [
    'et-blister' => [
        'pdf' => 'et-blister',
        'style' => 'blister',
    ],







    'et-sache' => [
        'pdf' => 'et-sache',
        'style' => 'et_sache',
        'buttons' => [
            [
                'name' => 'Rotulo Caixa',
                'goTo' => 'et-sache'
            ],
            [
                'name' => 'Rotulo Sache 70',
                'goTo' => 'sc70'
            ],
            [
                'name' => 'Rotulo Sache 140',
                'goTo'  => 'sc140',
            ],
        ]
    ],
];