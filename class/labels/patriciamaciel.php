<?php 
return [
    'alm_etgr-pmshg' => [
        'pdf' => 'pm_shampoo_gr',
        'style' => 'alm_shampoo',
        'extraCss' => 'alm_shampoo_eco', 
    ],

    'alm_r160-pmshg' => [
        'pdf' => 'pm_shampoo_gr',
        'style' => 'alm_shampoo',
        'extraCss' => 'alm_shampoo_eco', 
    ],

    'pmshcond' => [
        'pdf' => 'pm_condicionador_gr',
        'style' => 'alm_shampoo',
        'extraCss' => 'alm_shampoo_eco',
    ],

    'pmshcondpq' => [
        'pdf' => 'pm_condicionador_pq',
        'style' => 'alm_leavin',
        'extraCss' => 'pm_condicionador_pq',
    ],

    'pmshg' => [
        'pdf' => 'pm_shampoo_gr',
        'style' => 'alm_shampoo',
        'extraCss' => 'alm_shampoo_eco', 
    ],

    'alm_sh-cond-pmshg' => [
        'pdf' => 'pm_shampoo_gr',
        'style' => 'alm_shampoo',
        'extraCss' => 'alm_shampoo_eco', 
    ],

    'alm_sh-cnd-eccreg' => [
        'pdf' => 'pm_shampoo_pq',
        'style' => 'alm_shampoo',
        'extraCss' => 'alm_shampoo_eco',
    ],
    
    'pm_condicionador_gr' => [
        'pdf' => 'pm_condicionador_gr',
        'style' => 'alm_shampoo',
        'extraCss' => 'alm_shampoo_eco',
    ],

    'pmshp' => [
        'pdf' => 'pm_shampoo_pq',
        'style' => 'alm_leavin',
        'extraCss' => 'alm_shampoo_eco',
    ],

    'pmcndp' => [
        'pdf' => 'pm_condicionador_pq',
        'style' => 'alm_leavin',
    ],

    'pmtonico' => [
        'pdf' => 'pm_tonico',
        'style' => 'alm_leavin',
    ],

    'pmton' => [
        'pdf' => 'pm_tonico',
        'style' => 'alm_leavin',
    ],

    'pmleavin' => [
        'pdf' => 'pm_leavin',
        'style' => 'alm_leavin',
    ],

    'pmleav' => [
        'pdf' => 'pm_leavin',
        'style' => 'alm_leavin',
    ],

    'pmmask' => [
        'pdf' => 'pm_mascara',
        'style' => 'alm_mascara',
    ],
];