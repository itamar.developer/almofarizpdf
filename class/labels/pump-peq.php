<?php 
return [
    'pump-peq-medicamentos' => [
        'pdf' => 'pump-peq-medicamentos',
        'style' => 'pump_p',
        'buttons' => $buttonR80Medicamento = [
            [
                'name' => 'Original',
                'goTo' => 'pump-peq-medicamentos'
            ],
            [
                'name' => 'Tarja preta',
                'goTo' => 'pump-peq-medicamentos-tarja-preta'
            ],
            [
                'name' => 'Tarja vermelha 01',
                'goTo'  => 'pump-peq-medicamentos-tarja-vermelha-01',
            ],
            [
                'name' => 'Tarja vermelha 02',
                'goTo'  => 'pump-peq-medicamentos-tarja-vermelha-02',
            ],
            [
                'name' => 'Tarja vermelha 03',
                'goTo'  => 'pump-peq-medicamentos-tarja-vermelha-03',
            ],
        ]
    ],
    'pump-peq-medicamentos-tarja-preta' => [
        'pdf' => 'pump-peq-medicamentos-tarja-preta',
        'style' => 'pump_p',
        'extraCss' => ['pump_p', 'pump_p_tarja'],
        'buttons' => $buttonR80Medicamento
    ],
    'pump-peq-medicamentos-tarja-vermelha-01' => [
        'pdf' => 'pump-peq-medicamentos-tarja-vermelha-01',
        'style' => 'pump_p',
        'extraCss' => ['pump_p', 'pump_p_tarja'],
        'buttons' => $buttonR80Medicamento
    ],
    'pump-peq-medicamentos-tarja-vermelha-02' => [
        'pdf' => 'pump-peq-medicamentos-tarja-vermelha-02',
        'style' => 'pump_p',
        'extraCss' => ['pump_p', 'pump_p_tarja'],
        'buttons' => $buttonR80Medicamento
    ],
    'pump-peq-medicamentos-tarja-vermelha-03' => [
        'pdf' => 'pump-peq-medicamentos-tarja-vermelha-03',
        'style' => 'pump_p',
        'extraCss' => ['pump_p', 'pump_p_tarja'],
        'buttons' => $buttonR80Medicamento
    ],


    'pump-peq-dermocosmeticos' => [
        'pdf' => 'pump-peq-dermocosmeticos',
        'style' => 'pump_p',
        'extraCss' => ['pump_p', 'pump_p_tarja'],
    ],
    'pump-peq-nutraceuticos' => [
        'pdf' => 'pump-peq-nutraceuticos',
        'style' => 'pump_p',
    ],
    //PARAMETRO COSMPQ//
    'pump-peq-creme-maos' => [
        'pdf' => 'pump-peq-creme-maos',
        'style' => 'pump_p_creme',
    ],
    'pump-gr-creme-maos' => [
        'pdf' => 'pump-gr-creme-maos',
        'style' => 'pump_g_creme',
    ],
    'cosmpq' => [
        'pdf' => 'pump-peq-creme-maos',
        'style' => 'pump_p_creme',
    ],
];