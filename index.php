<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
require_once('./class/Labels.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
    </head>
    <body>
        <form action="labelMaker.php" method="post">
            Rotulo: 
            <select name="rotulo"> 
                <?php
                foreach(Labels::getLabels() as $name => $label){
                    echo "<option valie='$name'>$name</option>";
                }
                ?>
            
            </select>
            <p>
                Digite o rotulo: <input type="text" name="string_rotulo" />
            </p>
            Data: <textarea name="data" style="width: 100%; height:400px;">
{
    "code": "1-830390-0",
    "name":"Itamar l.",
    "qtdy":"60 cápsulas",
    "namereg":"SR. EVANDRO LUIS SOARES",
    "reg":"Reg 717043",
    "req":"Req 003018284",
    "flavor": "Laranja",
    "componentes":"TIAMINA 200mg; NIACINAMIDA 35mg; PANTO-TENATO CALCIO 3mg;VITAMINA B6 (PIRIDO-XINA) 20mg; CAFEINA 200mg; METILFOLATO 250mcg; METILCOBALAMINA 9mcg; TIROSINA 299mg; COENZIMA Q10 10mg; TAURINA 200mg; ARGININA 250 mg; GENGIBRE 100mg;",
    "posologia": "Qntd: 2x (90) caps. TOMAR 1 DOSE (2 CAPSULAS) ANTES DO TREINO.",
    "posologiaDesc": "3 caps no cfé da manha / 3 caps no almoço / 3 caps no jantar. Total: 9 caps ao dia.",
    "doctor":"DR. GUILHERME FERREIRA TAKASSI",
    "crm":"CRM: 123.345",
    "man":"Man: 07/11/2019",
    "val":"Val: 16/03/2020",
    "farmresp":"Patrícia da Silva Rodrigues Fernandes CRF: 23.353 / CNPJ 58.867.144/0001-20",
    "tituloFormula":"Titulo da formula",
    "qtdeProduto": "350ml",
    "ativosFormula": "IMUNOVITAE ® 500MG",
    "descrProduto" : "PANTO-TENATO CALCIO 3mg",
    "matriz":"Rua Turiassu, 1481, Perdizes, São Paulo (SP) CEP: 05005-000 / TEL: (11) 3298-5000"
}
            </textarea>
            <label for="pdf"> Output PDF ?</label>
            <input type="checkbox" id="pdf" name="pdf" value="true">
            <br />
            <br />
            <button type="input">Enviar</button>
        </form>
    </body>
</html>